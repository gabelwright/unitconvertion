package com.gabelwright.unitconversion;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity {

    EditText input;
    TextView answer;
    UnitConverter unitConverter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        unitConverter = new UnitConverter();
        setUnits();
        input = findViewById(R.id.main_input1);
        answer = findViewById(R.id.main_answer);



        input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                try{
                    String val = input.getText().toString();
                    double value = unitConverter.convert(Double.parseDouble(val));
                    DecimalFormat formatter = new DecimalFormat("###.##");
                    String final_ans = formatter.format(value);
                    answer.setText(final_ans);
                }catch(Exception e){
                    answer.setText("");
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.feet_to_meter) {
            unitConverter.setMode(UnitConverter.Mode.FEET_TO_METER);
            setUnits();
            return true;
        }
        else if (id == R.id.in_to_cm) {
            unitConverter.setMode(UnitConverter.Mode.INCH_TO_CENTIMETER);
            setUnits();
            return true;
        }
        else if (id == R.id.pound_to_gram) {
            unitConverter.setMode(UnitConverter.Mode.POUND_TO_GRAM);
            setUnits();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    private void setUnits(){
        TextView unit1 = findViewById(R.id.main_unit1);

        TextView unit2 = findViewById(R.id.main_unit2);

        unit1.setText(unitConverter.fromUnit());
        unit2.setText(unitConverter.toUnit());
        input = findViewById(R.id.main_input1);
        input.setText("");
    }
}
